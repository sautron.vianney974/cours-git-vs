#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "coded.h"

FILE *f_coded = NULL;
FILE *f_perroquet = NULL;

char *perroquet_coded_filename = NULL;
char *coded_coded_filename = NULL;
int idx_perroquet_file = 0;
int coded_eof = 1;

int is_eof()
{
	return coded_eof;
}

void coded_open(char *filename, char *mode)
{
	if (f_coded == NULL)
	{
		f_coded = fopen(filename, mode);
		if (f_coded == NULL)
		{
			printf("\n%s cannot be open!\n", filename);
			return;
		}
		coded_eof = 0;
		coded_coded_filename = malloc(strlen(filename) + 1);
		strcpy(coded_coded_filename, filename);
	}
	else
	{
		printf("\nMethod 'coded_open' aleady been called!\n");
	}
}

void coded_close()
{
	if (f_coded != NULL)
	{
		fclose(f_coded);
		f_coded = NULL;
		if (coded_coded_filename != NULL)
		{
			free(coded_coded_filename);
		}
	}
	if (f_perroquet != NULL)
	{
		if (perroquet_coded_filename != NULL)
		{
			free(perroquet_coded_filename);
		}
		fclose(f_perroquet);
		f_perroquet = NULL;
	}
}

void coded_set_perroquet(char *filename)
{
	if (f_perroquet == NULL)
	{
		f_perroquet = fopen(filename, "rb");
		if (f_perroquet == NULL)
		{
			printf("\n%s cannot be open!\n", filename);
			return;
		}
		perroquet_coded_filename = malloc(strlen(filename) + 1);
		strcpy(perroquet_coded_filename, filename);
	}
	else
	{
		printf("Method 'coded_set_perroquet' already been called!\n");
	}
}

char *coded_next_line()
{
	unsigned char buf[1] = {0};
	unsigned char peroq[1] = {0};
	int end_line = 0;
	char line[256];
	int idx = 0;
	while (!end_line)
	{
		int bytes = fread(buf, sizeof *buf, 1, f_coded);
		if (bytes != 0)
		{
			if (fread(peroq, sizeof *peroq, 1, f_perroquet) == 0)
			{
				fclose(f_perroquet);
				f_perroquet = fopen(perroquet_coded_filename, "rb");
				fread(peroq, sizeof *peroq, 1, f_perroquet);
			}
			line[idx] = buf[0] + peroq[0];
			if (line[idx] == '\n')
			{
				end_line = 1;
			}
			idx++;
		}
		else
		{
			end_line = 1;
			coded_eof = 1;//end of file
		}
	}
	if (idx > 0)
	{
		line[idx - 1] = '\0';
	}
	char *return_string;
	return_string = malloc(idx);
	strcpy(return_string, line);
	return return_string;
}

void coded_add_line(char *line)
{
	unsigned char buf[1] = {0};
	unsigned char peroq[1] = {0};
	int end_file = 0;

	while (!end_file)
	{
		int bytes = fread(buf, sizeof *buf, 1, f_coded);
		if (bytes != 0)
		{
			if (fread(peroq, sizeof *peroq, 1, f_perroquet) == 0)
			{
				fclose(f_perroquet);
				f_perroquet = fopen(perroquet_coded_filename, "rb");
				fread(peroq, sizeof *peroq, 1, f_perroquet);
			}
		}
		else
		{
			end_file = 1;
		}
	}

	f_coded = fopen(coded_coded_filename, "ab+");

	for (int i = 0; i < strlen(line); i++)
	{
		if (fread(peroq, sizeof *peroq, 1, f_perroquet) == 0)
		{
			fclose(f_perroquet);
			f_perroquet = fopen(perroquet_coded_filename, "rb");
			fread(peroq, sizeof *peroq, 1, f_perroquet);
		}
		buf[0] = line[i] -  peroq[0];
		fwrite(buf, 1, 1, f_coded);
	}
}
