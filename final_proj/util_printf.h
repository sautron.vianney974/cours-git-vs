#ifndef _UTILS_PRINTF_H_
#define _UTILS_PRINTF_H_

#include <stdio.h>

#define BLACK 30
#define RED 31
#define GREEN 32
#define YELLOW 33
#define BLUE 34
#define MAGENTA 35
#define CYAN 36
#define WHITE 37

#define BK_BLACK 40
#define BK_RED 41
#define BK_GREEN 42
#define BK_YELLOW 43
#define BK_BLUE 44
#define BK_MAGENTA 45
#define BK_CYAN 46
#define BK_WHITE 47


#define PRINTLN(str, color) printf("\033[1;%dm%s\033[0m\n", color, str);
#define PRINTBKLN(str, color, bk) printf("\033[5;%dm\033[1;%dm%s\033[0m\n", bk, color, str);
#define PRINT(str, color) printf("\033[1;%dm%s\033[0m", color, str);
#define PRINTBK(str, color, bk) printf("\033[5;%dm\033[1;%dm%s\033[0m", bk, color, str);


#endif
