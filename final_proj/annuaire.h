#ifndef _ANNUAIRE_H_
#define _ANNUAIRE_H_

#include <stdio.h>

typedef struct phone_user
{
	char nom[100];
	char prenom[100];
	char tel[20];
}s_phone_user;

int add_new();
void view(char*);
void search();
void view_coded(char*);
void search_coded();
int add_new_coded();

void setup_coded();

void destroy_annuaire();

int is_coded_set();

#endif
