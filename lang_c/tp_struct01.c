#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct vehicule()
{
	char marque[256];
	char modele[256];
	int gazole;
	int v_max;
	char etat[256];
}

int fonction()
{
	struct vehicule v1 = {Renault, Clio II, 100, 200, 'fonctionnel'};
	printf("nom du vehicule : %c \n", v1.marque);
	printf("modele : %c \n", v1.modele);
	printf("gazole : %d \n", v1.gazole);
	printf("vitesse max : %d \n", v1.v_max);
	printf("etat : %c \n", v1.etat);

return 0;
}


