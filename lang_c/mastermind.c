#include <stdio.h>
#include <stdlib.h>

int main() 
{
	char saisie[4] = {'O', 'J', 'B', 'V'}; // saisie utilisateur
	char code[4] = {'R', 'V', 'B', 'J'};  // code secret à deviner
	int c = 3; // compteur d'essai, initialisé à 3 pour 3 essais
	int l_b = 0; // compteur de lettres bonnes
	int l_m = 0; // compteur de lettres mauvaises
	int i; // compteur i de la boucle for

	printf("Donnez un code de 4 couleurs differentes et sans espaces parmis : {'R', 'V', 'B', 'J', 'O'}\n");
	
	while (c != 0) // permet de rejouer tant qu'il y a des tentatives restantes
	{
		fflush(stdin);
		scanf("%s", saisie); // l'utilisateur entre sa saisie

		for (i == 0; i < 5; i++) // on compare chaque élément de chaque tableau un par un
		{
			if (saisie[i] == code[i]) 
			{
				l_b += 1; // si les éléments i de chaque tableau sont identiques, on incrémente le compteur de bonne lettres

			}
			else
			{
				l_m += 1; // dans le cas contraire, on incrémente le compteur de lettres mauvaises
			}
		}

		c -= 1; // on décrémente le compteur de tentatives après chaque tentative
		if (l_m == 0) // cela signifie que le code saisie est le bon code
		{
			printf("Vous avez trouvé le bon code !\n");
			c = 0;
		}
		else //l'utilisateur n'a pas saisie le bon code
		{
			printf("Vous n'avez pas encore trouvé le bon code ! \n");
			printf("Il vous reste %d essais\n", c);
		}
		printf("Lettres bonnes = %d \n", l_b); // un affiche le nombre de lettres bonnes
		printf("Lettres mauvaises = %d \n", l_m); // on affihe le nombre de lettres mauvaises

	}		

	return 0;
}

