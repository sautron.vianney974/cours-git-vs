#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	// Déclaration et initialisation des variables
	int juste_prix = 0;
	int prix = 0;
	signed int c = 10;
	const int VALEUR_MIN = 1, VALEUR_MAX = 100;
	int replay = 1;

	// Génération d'un nombre aléatoire
	srand(time(NULL));
	juste_prix = (rand() % (VALEUR_MAX - VALEUR_MIN + 1)) + VALEUR_MIN;

	//Corps du programme
	while (replay == 1) //permet de gérer si le joueur veut relancer une partie
	{

	//Paramètrages du jeu
	printf("Combien de tentatives max vous autorisez-vous?\n");
	scanf("%d", &c);


	//Début du jeu
	printf("Le juste prix est compris entre 0 et 100, à vous de le deviner : \n");
	scanf("%d", &prix);
	
	while (c > 1) // c > 1 car avec c > 0 il y avait c + 1 tentatives
	{
		if (prix > juste_prix)
		{
			printf("Non, c'est moins \n");
			c -= 1; // décrémentation du compteur
		}
		else if (prix == juste_prix)
		{

			c = -1; //-1 est le code sortie gagnant de l'algorithme

		}
		else if (prix < juste_prix)
		{
			printf("Non, c'est plus\n");
			c -= 1;
		}
		if (c != -1)
		{
		printf("Réessayez, il vous reste %d essai(s),\n", c);
		scanf("%d", &prix);
		}
	}
	if (c == 1)
	{

		printf("Time out ! Vous avez utilisé toutes vos tentatives, c'est perdu\n");
		printf("Voulez-vous rejouer ? (1) oui / (2) non \n");
		scanf("%d", &replay); // le joueur choisit de rejouer ou non
	}
	else if (c == -1) // donc c = -1 , le code gagnant de sortie de l'algorithme
	{ 
		printf("Oui c'est gagné !!!\n");
		printf("Voulez-vous rejouer ? (1) oui / (2) non\n");
		scanf("%d", &replay);
	}
	
}

return 0;
}

