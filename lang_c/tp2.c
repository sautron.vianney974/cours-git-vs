#include <stdio.h>

int main()
{
	int My_int = 1;
	long My_long = 100;
	short My_short = 2;
	float My_float = 10.3;
	double My_double = 10.33;
	char My_char = 3;

	printf("%d\n", My_int);
	printf("%ld\n", My_long);
	printf("%d\n", My_short);
	printf("%f\n", My_float);
	printf("%f\n", My_double);
	printf("%c\n", My_char);
	return 0;
}
