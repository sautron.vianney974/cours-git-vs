#include <stdio.h>
#include <stdlib.h>

int main()
{
	char mot_secret[3] = {'-', '-', '-'};
	char saisie_utilisateur;
	int vies = 10;


	printf("Devinez le mot suivant : %c \n", mot_secret);
	printf("Proposez une lettre\n");

	fflush(stdin);
	scanf("%c", &saisie_utilisateur);

while (vies != 0 || mot_secret[3] != 'L', 'O', 'L')
{

	if (saisie_utilisateur == 'L')
	{
		printf("Bien joué, la lettre L est effectivement présente dans le mot\n");
		mot_secret[0] = 'L';
		mot_secret[2] = 'L';
		printf("Devinez le mot suivant : %c \n", mot_secret);
		printf("Proposez une lettre : \n");
		fflush(stdin);
		scanf("%c", &saisie_utilisateur);

	}

	else if (saisie_utilisateur == 'O')
	{
		printf("Bien joué, la lettre O est effectivement présente\n");
		mot_secret[1] = 'O';
		printf("Devinez le mot suivant : %c \n", mot_secret);
		printf("Proposez une lettre : \n");
		fflush(stdin);
		scanf("%c", &saisie_utilisateur);
	}
	else
	{
		vies -= 1;
		printf("Dommage, cette lettre n'est pas présente\n");
		printf(" Il vous reste %d vies\n", vies);
		printf("Devinez le mot suivant : %c \n", mot_secret);
		printf("Proposez une lettre : \n");
		fflush(stdin);
		scanf("%c", &saisie_utilisateur);
	}

}

return 0;

}

