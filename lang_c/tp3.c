#include <stdio.h>
#include <stdlib.h>

int main()
{
	int cote_carre = 0;
	int perimetre_carre = 0;
	int surface_carre = 0;
	printf("Quelle est la taille d'un cote du carre ?\n");
	scanf("%d", &cote_carre);
	perimetre_carre = 4*cote_carre;
	surface_carre = cote_carre*cote_carre;
	printf("Ce carre a pour perimetre %d\n", perimetre_carre);
	printf("Ce carre a pour surface %d\n", surface_carre);
	return 0;
}

